// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// const baseUrl = 'http://192.168.1.36:3000/';

const baseUrl = 'http://ec2-18-139-160-136.ap-southeast-1.compute.amazonaws.com:3000/';

export const environment = {
  baseUrlUse : 'http://192.168.1.36:3000/',

  production: false,

  login: `${baseUrl}auth/login`,

  createFranchise: `${baseUrl}franchise/createfranchise`,

  addStoreUser: `${baseUrl}storeuser/addstoreuser`,
  findRoleStoreUsers: `${baseUrl}storeuser/findrolestoreusers`,
  franchiseAddProduct: `${baseUrl}franchiseproduct/addproduct`,

  addCategory: `${baseUrl}category/addcategory`,
  addsubcategory: `${baseUrl}category/addsubcategory`,

  findallcategories: `${baseUrl}category/allcategories`,

  findFrenchiseProduct: `${baseUrl}franchiseproduct/findfranchiseproduct`,
  allProducts: `${baseUrl}products/allproducts`,
  addGenericProducts: `${baseUrl}products/addproduct`,
  viewallsuppliers: `${baseUrl}supplier/viewsuppliers`,
  editProduct: `${baseUrl}products/editproduct`,
  editCategory: `${baseUrl}category/editcategory`,


  viewFranchiseSuppliers: `${baseUrl}supplier/viewfranchisesuppliers`,
  addsuppliers: `${baseUrl}supplier/addfranchisesupplier`,
  editSupplier: `${baseUrl}supplier/editsupplier`,
  //Store API
  deleteStore: `${baseUrl}store/deletestore`,
  createStore: `${baseUrl}store/createstore`,
  allStores: `${baseUrl}store/allstores`,

  findallstoreusers: `${baseUrl}storeuser/findallstoreusers`,

  //franchise API
  deleteFranchise: `${baseUrl}franchise/deletefranchise`,
  allFranchises: `${baseUrl}franchise/findallfranchises`,
  findFrenchiseProducts: `${baseUrl}franchiseproduct/findfranchiseproducts`,
  deleteStoreUser: `${baseUrl}storeuser/deletestoreuser`,

  //invoice
  createsalesinvoice: `${baseUrl}invoice/createinvoice`,
  findinvoice: `${baseUrl}invoice/findinvoice`,
  allpurchaseinvoices: `${baseUrl}invoice/allpurchaseinvoices`,
  allsaleinvoices: `${baseUrl}invoice/allsaleinvoices`,
};

/*
 * For easier debugging in development mode, you can import the following file
//  * to ignore zone related error stack frames such as
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
