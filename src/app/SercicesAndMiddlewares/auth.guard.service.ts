import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs'
import { map, take } from 'rxjs/operators'
import { Store, select } from '@ngrx/store'
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private _store: Store<any>, private router: Router) { }

  canActivate(): Observable<boolean>{
    return this._store.pipe(
      select('states'),
      map(authed => {
        if(!authed.loggedin){
          this.router.navigate(['/','admin','dashboard','home'])
          return false;
        } else {
          return true;
        }
      }),
      take(1));
  }
}
