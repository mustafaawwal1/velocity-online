const initialState = {
    loggedin: false,
    userData: {},
    currentLocation: {}
}
export function states(state = initialState, action){
    switch (action.type){
        case 'LOGIN':
            return {...state, userData: action.payload, loggedin: true};
        case 'LOGOUT':
              return initialState;
        case 'CURRENT_LOCATION':
            return {...state, currentLocation: action.payload};
        default:
            return state;
    }
}
