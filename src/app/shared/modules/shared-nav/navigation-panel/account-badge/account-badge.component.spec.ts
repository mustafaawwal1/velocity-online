import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountBadgeComponent } from './account-badge.component';

describe('AccountBadgeComponent', () => {
  let component: AccountBadgeComponent;
  let fixture: ComponentFixture<AccountBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
