import { Component, OnInit } from '@angular/core';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-account-badge',
  templateUrl: './account-badge.component.html',
  styleUrls: ['./account-badge.component.scss']
})
export class AccountBadgeComponent implements OnInit {
  state = {
    name: 'Muhammad Mustafa Awwal',
    branchName: 'Abbottabad',
    expandedTrue: false
  }
  iconPack = {
    logoutLogo: faSignOutAlt
  }
  constructor(private router: Router,private _store:Store<any>) { }

  ngOnInit() {
  }
  expand() {
    this.state.expandedTrue = !this.state.expandedTrue;
  }
  logoutStore() {
    this._store.dispatch({
      type: 'LOGOUT'
    });
    this.router.navigate(['/']);
  }
}
