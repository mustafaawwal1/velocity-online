import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TableActionComponent } from './table-action/table-action.component';
import { RouterModule } from '@angular/router';
import { PopupFormComponent } from './popup-form/popup-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [TableActionComponent, PopupFormComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    FontAwesomeModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
 
  exports: [
    TableActionComponent,
    PopupFormComponent
  ]
})
export class SharedComponentsModule { }
