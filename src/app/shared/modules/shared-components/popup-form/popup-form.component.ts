import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-popup-form',
  templateUrl: './popup-form.component.html',
  styleUrls: ['./popup-form.component.scss']
})
export class PopupFormComponent implements OnInit {
  formTitle = ''
  visibleState = false;
  image: String;
  genericForm: FormGroup;
  formLabels = [];
  iconPack = {
    cross: faTimes
  }
  @Output() formResult = new EventEmitter<any>()
  constructor(private fb:FormBuilder) { }
  

  ngOnInit() {
  }
  visibleStateToggler() {
    this.visibleState = !this.visibleState;
  }

  formFiller(formData) {
    this.genericForm.patchValue(formData)
  }
  submit(formData) {
    console.log(formData)
    this.formResult.emit(formData)
  }
}
