import { environment } from 'src/environments/environment';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-new-purchase',
  templateUrl: './new-purchase.component.html',
  styleUrls: ['./new-purchase.component.scss']
})
export class NewPurchaseComponent implements OnInit {
  allFranchises: any;
  get productArray() {
    return  this.newPurchaseForm.get('products') as FormArray;
  }
  productListQuantity = 0;
  iconPack = {
    plus: faPlus,
    cross: faTimes
  };
  suppliers = [];
  //   {
  //     name: 'something'
  //   },
  //   {
  //     name: 'something else'
  //   }
  // ];
  billTypes = [
    {
      value: 'Cash Sale'
    },
    {
      value: 'Credit Sale'
    },
    {
      value: 'Quotation'
    },
    {
      value: 'Sales Return'
    }
  ];
   productList = []
  //   {
  //     name: 'Pepsi',
  //     value: 'Pepsi',
  //     code: 33301,
  //     unitPrice: '300'
  //   },
  //   {
  //     name: 'pizza',
  //     value: 'pizza',
  //     code: 33302,
  //     unitPrice: '3000'
  //   }
  // ]
  paymentMethods = [
    {
      type: 'cash',
    },
    {
      type: 'cheque'
    },
    {
      type: 'others'
    }
  ];
  newPurchaseForm: FormGroup;
  constructor(private fb: FormBuilder, private api: ApiWrapperService) { }

  ngOnInit() {
    this.createForm();
    this.api.get(environment.allProducts).subscribe(res => {
      //this.productList = res.products;
      this.productList = res.products.map((product) => {
        product.value = product.name;
        product.code = product.productCode;
        product.unitPrice = product.costPrice;
        return product;
      })
      console.log(this.productList);
    });

    // this.api.get(environment.allFranchises).subscribe(response => {
    //   console.log(response.franchises);
    //   this.allFranchises = response.franchises;

    // });

    this.api.post(environment.viewFranchiseSuppliers).subscribe(res => {
          console.log(res.suppliers);
          this.suppliers = res.suppliers;
          });

  }




  createForm() {
    this.newPurchaseForm = this.fb.group({
      franchiseId: ['', [Validators.required]],
      supplierId: ['', [Validators.required]],
      total: ['', [Validators.required]],
      discount: ['', [Validators.required]],
      shippingCost: ['', [Validators.required]],
      netTotal: ['', [Validators.required]],
      paymentMethod: ['', [Validators.required]],
      paid: ['', [Validators.required]],
      date: ['', [Validators.required]],
      invoiceNo: [1, [Validators.required]],
      billType: ['', [Validators.required]],
      products: this.fb.array([
      ])
    });
  }
  get products() {
    return this.newPurchaseForm.get('products') as FormArray;
  }
  createProduct(): FormGroup {
    return this.fb.group({
      productId: ['', [Validators.required]],
      productName: ['', [Validators.required]],
      unitPrice: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      subTotal: ['', [Validators.required]]
    });
  }

  deleteProduct(index) {
    this.products.removeAt(index);
  }
  submitForm(data) {
    console.log(data);
    this.api.post(environment.franchiseAddProduct,data).subscribe(res => {
      console.log(res);
      this.ngOnInit();
    })
  }
  addSingleProduct(data) {
    const group: FormGroup = this.createProduct();
    group.patchValue(data);
    this.products.push(group);
    this.calculatingNetValue();
  }
  calculatingNetValue() {
    const rawValue = this.products.getRawValue();
    let netTotal = 0;
    for (const entry of rawValue) {
      netTotal += entry.subTotal;
    }
    this.newPurchaseForm.patchValue({
      netTotal
    });
  }
  updateFormProduct(data, index) {
    console.log(data.value);

    const product = this.productList.find(x => x.value === data.value);
    // this.saleSingleForm.patchValue({
    //   unitPrice: product.unitPrice,
    //   subTotal: product.unitPrice*this.quantity
    // })
    console.log('ppp',product);
    this.products.controls[index].patchValue({
      unitPrice: product.costPrice
    });
    this.quantityChange(index);
  }
  quantityChange(index) {
    const currentGroup = this.products.controls[index] as FormGroup;
    const unitPrice  = currentGroup.controls.unitPrice.value;
    const quantity   = currentGroup.controls.quantity.value;
    const subTotal   = unitPrice * quantity;
    currentGroup.patchValue({
      subTotal
    });
    this.calculatingNetValue();
  }


  // franchiseSuppliers(){
  //   console.log('kjjjjjjjjj',this.newPurchaseForm.controls['franchiseId'].value);
  //   this.api.post(environment.viewFranchiseSuppliers,{franchiseId:this.newPurchaseForm.controls['franchiseId'].value}).subscribe(res => {
  //     console.log(res.suppliers);
  //     this.suppliers = res.suppliers;
  //     });
  // }
}

