import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-purchase-list',
  templateUrl: './purchase-list.component.html',
  styleUrls: ['./purchase-list.component.scss']
})
export class PurchaseListComponent implements OnInit {
  iconPack = {
    plus: faPlus
  };
  purchaseData = [];
  allFranchises: any;
  checker: boolean=false;
  messageChecker: boolean=false
  isLoading: boolean;
  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.checker=false;
    this.isLoading = true;
    this.api.post(environment.allpurchaseinvoices).subscribe(res => {
      console.log(res.invoices);
      this.purchaseData = res.invoices;
      this.isLoading = false;
      this.checker=true;
    });
  }
  // franchiseId(event: any) {
  //   this.isLoading = true;

  //   this.checker=false;
  //   this.api.post(environment.allpurchaseinvoices).subscribe(res => {
  //     console.log(res.invoices);
  //     this.purchaseData = res.invoices;
  //     this.checker = true;
  //     this.messageChecker =true;
  //     this.isLoading = false;
  //     // tslint:disable-next-line: no-unused-expression
  //     }, (err: any) => {
  //       console.log('error_message : ', err.error);
  //       this.messageChecker =true;
  //       this.purchaseData=[];
  //       this.isLoading = false;
  //     });
  // }
}
