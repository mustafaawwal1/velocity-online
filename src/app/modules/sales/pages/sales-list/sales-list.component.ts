import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sales-list',
  templateUrl: './sales-list.component.html',
  styleUrls: ['./sales-list.component.scss']
})
export class SalesListComponent implements OnInit {
  iconPack = {
    plus: faPlus
  }
  saleInvoices: any;
  checker: boolean;
  isLoading: boolean;
  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.isLoading = true;
    this.checker = false;
    this.api.post(environment.allsaleinvoices).subscribe(res => {
      this.saleInvoices = res.invoices;
      this.checker = true;
      this.isLoading = false;
    })
  }

}
