import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',
  styleUrls: ['./new-sale.component.scss']
})
export class NewSaleComponent implements OnInit {
  allFranchises: any;
  get productArray() {
    return  this.newSaleForm.get('products') as FormArray;
  }
  iconPack = {
    plus: faPlus,
    cross:  faTimes
  };
  billTypes = [
    {
      value: 'Cash Sale'
    },
    {
      value: 'Credit Sale'
    },
    {
      value: 'Quotation'
    },
    {
      value: 'Sales Return'
    }
  ];
  customerTypes = [
    {
      type: 'Walk in Customer',
      value: 'walk in'
    }
  ];
  productList = [];
  paymentMethods = [
    {
      type: 'cash',
    },
    {
      type: 'cheque'
    },
    {
      type: 'others'
    }
  ];
  newSaleForm: FormGroup;
  constructor(private fb: FormBuilder, private api: ApiWrapperService) { }

  ngOnInit() {
    this.createForm();

    // this.api.get(environment.allFranchises).subscribe(response => {
    //   console.log(response.franchises);
    //   this.allFranchises = response.franchises;

    // });

    this.api.post(environment.findFrenchiseProducts).subscribe(res => {
      // tslint:disable-next-line: comment-format
      //this.productList = res.products;


      // console.log(res.product[0].productId);

      this.productList = res.product.map((product) => {
        product.value = product.productId.name;
        product.code = product.productId.productCode;
        product.unitPrice = product.productId.costPrice;
        return product;

      });

      console.log(this.productList);
    });
  }
  createForm() {
    this.newSaleForm = this.fb.group({
      franchiseId: ['', [Validators.required]],
      customerType: ['', [Validators.required]],
      total: ['', [Validators.required]],
      discount: ['', [Validators.required]],
      shippingCost: ['', [Validators.required]],
      netTotal: ['', [Validators.required]],
      paymentMethod: ['', [Validators.required]],
      paid: ['', [Validators.required]],
      date: ['', [Validators.required]],
      invoiceNo: [1, [Validators.required]],
      billType: ['', [Validators.required]],
      products: this.fb.array([
      ])
    });
  }

  createProduct() {
    return this.fb.group({
      productId: ['', [Validators.required]],
      productName: ['', [Validators.required]],
      unitPrice: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      subTotal: ['', [Validators.required]],
    });
  }
  get products() {
    return this.newSaleForm.get('products') as FormArray;
  }

  deleteProduct(index) {
    this.products.removeAt(index);
  }
  submitForm(data) {
    console.log(data);
    this.api.post(environment.createsalesinvoice, data).subscribe(res => {
      console.log(res);
      this.ngOnInit();
    });
  }
  addSingleProduct(data) {
    const group: FormGroup = this.createProduct();
    group.patchValue(data);
    this.products.push(group);
    this.calculatingNetValue();
  }
  calculatingNetValue() {
    const rawValue = this.products.getRawValue();
    let netTotal = 0;
    for (const entry of rawValue) {
      netTotal += entry.subTotal;
    }
    this.newSaleForm.patchValue({
      netTotal
    });
  }
  updateFormProduct(data, index) {
    const product = this.productList.find(x => x.value == data.value);
    // this.saleSingleForm.patchValue({
    //   unitPrice: product.unitPrice,
    //   subTotal: product.unitPrice*this.quantity
    // })
    this.products.controls[index].patchValue({
      unitPrice: product.unitPrice
    });
    this.quantityChange(index);
  }
  quantityChange(index) {
    const currentGroup = this.products.controls[index] as FormGroup;
    const unitPrice  = currentGroup.controls.unitPrice.value;
    const quantity   = currentGroup.controls.quantity.value;
    const subTotal   = unitPrice * quantity;
    currentGroup.patchValue({
      subTotal
    });
    this.calculatingNetValue();
  }

  // franchiseProducts() {

  //   console.log('kjjjjjjjjj', this.newSaleForm.controls.franchiseId.value);
  //   this.api.post(environment.findFrenchiseProducts, {franchiseId: this.newSaleForm.controls.franchiseId.value}).subscribe(res => {
  //     // tslint:disable-next-line: comment-format
  //     //this.productList = res.products;
  //     let length = res.product.length;

  //     // console.log(res.product[0].productId);

  //     this.productList = res.product.map((product) => {
  //       product.value = product.productId.name;
  //       product.code = product.productId.productCode;
  //       product.unitPrice = product.productId.costPrice;
  //       return product;

  //     });

  //     console.log(this.productList);
  //   });
  // }
}
