import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-create-branch',
  templateUrl: './create-branch.component.html',
  styleUrls: ['./create-branch.component.scss']
})
export class CreateBranchComponent implements OnInit {

  visibleState = false;
  createBranchForm: FormGroup;
  iconPack = {
    cross: faTimes
  }
  constructor(private fb:FormBuilder,private api:ApiWrapperService) { }

  ngOnInit() {
    this.createForm()
  }
  visibleStateToggler() {
    this.visibleState = !this.visibleState;
  }
  createForm() {
    this.createBranchForm = this.fb.group({
      franchiseName: ['',Validators.required],
      address: ['',Validators.required],
      phone: ['',Validators.required],


    })
  }
  formFiller(formData) {
    this.createBranchForm.patchValue(formData)
  }

  submit(){
this.api.post(environment.createFranchise,this.createBranchForm.value).subscribe(res => {
  console.log(res);
  this.visibleState = !this.visibleState;
})
  }
}
