import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { faEdit } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {
  @ViewChild(MatPaginator,{static: true}) paginator: MatPaginator
  @ViewChild(MatSort,{static:true}) sort: MatSort;
  @Input() incomingData: any;
  dataSource: any;
  iconPack = {
    edit: faEdit
  }
  columnsToDisplay = ['id','name','email','role','actions']
  constructor() { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.incomingData);
    console.log(this.dataSource);
    this.dataSource.paginator = this.paginator
    this.dataSource.sort      = this.sort
  }
  editHandler(row) {

  }
  detailHandler(row) {

  }
  searchFilter(searchFilter) {
    this.dataSource.filter = searchFilter
  }
}
