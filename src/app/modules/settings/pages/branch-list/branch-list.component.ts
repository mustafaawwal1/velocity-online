import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { environment } from './../../../../../environments/environment';
import { CreateBranchComponent } from './../../components/create-branch/create-branch.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { SupplierLedgerFormComponent } from 'src/app/modules/accounts/components/supplier-ledger-form/supplier-ledger-form.component';

@Component({
  selector: 'app-branch-list',
  templateUrl: './branch-list.component.html',
  styleUrls: ['./branch-list.component.scss']
})
export class BranchListComponent implements OnInit {
  @ViewChild(CreateBranchComponent, {static: true}) form: CreateBranchComponent;
  iconPack = {
    plus: faPlus
  }
  tableData = []
  checker: boolean = false;
  isLoading: boolean;
  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.isLoading = true;
    this.checker = false;
    this.api.get(environment.allFranchises).subscribe( res => {
      console.log(res);
      this.tableData = res.franchises,
      this.checker = true;
      this.isLoading = false;

    })

  }
  openForm() {

    this.form.visibleStateToggler();

  }
}
