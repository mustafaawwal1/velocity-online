import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  iconPack = {
    plus: faPlus
  }
  tableData = []
  checker: boolean;
  isLoading: boolean;
  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.isLoading = true;
    this.checker = false
    this.api.get(environment.findallstoreusers).subscribe(res => {
      this.checker = true;
      console.log(res.storeUsers);
      this.tableData = res.storeUsers;
      this.isLoading = false;
    })
  }
  createHandlerFunction() {
  }
}
