import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  newStoreUser: any;
  allFranchises: any;

  constructor(private fb: FormBuilder, private api: ApiWrapperService,private router:Router) { }

  ngOnInit() {
this.newStoreUser = this.fb.group({
  franchiseId:[''],
  firstName:[''],
  lastName:[''],
  email:[''],
  password:[''],
  confirmPassword:[''],
  role:[''],
  phone:[''],
  address:[''],
})

this.api.get(environment.allFranchises).subscribe(response => {
  console.log(response.franchises);
  this.allFranchises = response.franchises;

});
  }


  onSubmit() {
this.api.post(environment.addStoreUser,this.newStoreUser.value).subscribe(res => {
console.log(res);
this.router.navigate(['/','admin','settings','generalSettings','user'])
});
  }


}
