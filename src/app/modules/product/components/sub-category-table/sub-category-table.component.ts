import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { PopupFormComponent } from 'src/app/shared/modules/shared-components/popup-form/popup-form.component';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sub-category-table',
  templateUrl: './sub-category-table.component.html',
  styleUrls: ['./sub-category-table.component.scss']
})
export class SubCategoryTableComponent implements OnInit {
  @Input() incomingData: any;
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:true}) sort: MatSort;
  @ViewChild(PopupFormComponent,{static:true}) form: PopupFormComponent;
  dataSource: MatTableDataSource<any>;
  iconPack = {
    edit: faEdit,
    delete: faTrash
  }
  columnsToDisplay= ['id','name','category','actions']
  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.incomingData)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort      = this.sort;
  }
  editHandler(row) {
    this.form.genericForm = this.fb.group({
      _id: ['',Validators.required],
      name: ['',Validators.required],
      subCategories: ['',Validators.required],
    })
    this.form.formLabels = [
      {placeholder:'id',name:'_id'},
      {placeholder:'Name',name:'subCategories'},
      {placeholder:'Category',name:'name'},
    ]
    this.form.formTitle = 'Sub Category Edit Form'
    this.form.visibleStateToggler()
    this.form.formFiller(row)
  }
  deleteHandler(row) {
  }
  searchFilter(searchText) {
    this.dataSource.filter = searchText
  }
}
