import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-new-category',
  templateUrl: './new-category.component.html',
  styleUrls: ['./new-category.component.scss']
})
export class NewCategoryComponent implements OnInit {
  showStatus = false;
  categoryForm: any;

  constructor(private fb: FormBuilder, private api: ApiWrapperService) { }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      categoryName: [''],
    });
  }
  statusChanger() {
    this.showStatus = !this.showStatus;
  }

  submit() {
    console.log(this.categoryForm.value);
    this.api.post(environment.addCategory, this.categoryForm.value).subscribe(res => {
console.log(res);
this.ngOnInit();
this.showStatus = !this.showStatus;
});
  }
}
