import { environment } from './../../../../../environments/environment';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import { PopupFormComponent } from 'src/app/shared/modules/shared-components/popup-form/popup-form.component';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnInit {
  columnsToDisplay: String[] = ['id', 'productCode','image', 'name', 'unit','costPrice','minimumRetailPrice', 'inStock', 'status', 'Actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @Input() incomingData: any;
  @ViewChild(PopupFormComponent,{static:true}) form: PopupFormComponent;
  dataSource: any;
  hostname: string;
  iconPack = {
    image: faImage
  }
  constructor(private fb: FormBuilder,private api: ApiWrapperService) { }
  ngOnInit() {

    this.hostname = environment.baseUrlUse;
    console.log('::::::::::::::', this.hostname);
    this.dataSource = new MatTableDataSource(this.incomingData);
    console.log('ddddd', this.incomingData);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort      = this.sort;

  }
  editHandler(row) {
    let tempRow = {...row}
    tempRow.category = tempRow.category.name
    
    this.form.genericForm = this.fb.group({
      productCode: ['',[Validators.required]],
      name: ['',[Validators.required]],
      unit: ['',[Validators.required]],
      costPrice: ['',[Validators.required]],
      minimumRetailPrice: ['',[Validators.required]],
      category: ['',[Validators.required]],
      status: ['',[Validators.required]],
      maximumQuantity: ['',[Validators.required]],
      minimumQuantity: ['',[Validators.required]],
      description: ['',[Validators.required]],
      productTax: ['',[Validators.required]],
      imageField: [''],
      '_id': ['',[Validators.required]]
    })
    this.form.formTitle = 'Product Edit Form'
    this.form.formLabels = [
      {name:'productCode',placeholder:'Product Code'},
      {name:'name',placeholder:'Name'},
      {name:'unit',placeholder:'Unit'},
      {name:'costPrice',placeholder:'Cost Price'},
      {name:'minimumRetailPrice',placeholder:'MRP'},
      {name:'category',placeholder:'Category'},
      {name:'status',placeholder:'Status'},
      {name:'minimumQuantity',placeholder: 'Min Qty'},
      {name:'maximumQuantity',placeholder: 'Max Qty'},
      {name:'description',placeholder: 'Description'},
      {name:'productTax',placeholder: 'Product Tax'},
      {name:'imageField',placeholder: 'Image Field'},


    ]
    this.form.visibleStateToggler();
    this.form.formFiller(tempRow)
  }

  searchFilter(searchFilter) {
    this.dataSource.filter = searchFilter;
  }
  submitForm(data) {
    console.log(data)
    this.api.post(environment.editProduct,data).subscribe((res)=> {
      console.log(res)
    })
    this.form.visibleStateToggler()
  }
}
