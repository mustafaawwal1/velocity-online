import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { PopupFormComponent } from 'src/app/shared/modules/shared-components/popup-form/popup-form.component';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit {
  @Input() incomingData: any;
  @ViewChild(MatPaginator,{static: true}) paginator: MatPaginator
  @ViewChild(MatSort,{static:true}) sort: MatSort;
  @ViewChild(PopupFormComponent,{static:true}) form:PopupFormComponent;
  dataSource: any;
  columnsToDisplay = ['id','name','subCategory','actions']
  iconPack = {
    edit: faEdit,
    delete: faTrash
  }
  constructor(private fb: FormBuilder,private api: ApiWrapperService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.incomingData);
    this.dataSource.paginator = this.paginator
    this.dataSource.sort      = this.sort
  }
  editHandler(row) {
   this.form.genericForm = this.fb.group({
    name: ['',[Validators.required]],
    _id: ['',[Validators.required]],

   })
   this.form.formLabels = [
     {name:"name",placeholder:'Category Name'},
   ]
   this.form.formTitle = 'Category Edit Form'
   this.form.visibleStateToggler()
   this.form.formFiller(row)
  }
  submitForm(data) {
    console.log(data)
    this.api.post(environment.editCategory,data).subscribe((res)=> {
      console.log(res)
    })
    this.form.visibleStateToggler()
  }
  deleteHandler(row) {
  }
  searchFilter(filterText:String) {
    this.dataSource.filter = filterText.trim().toLowerCase()
  }

}
