import { FormBuilder, Validators } from '@angular/forms';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { environment } from 'src/environments/environment';
import { PopupFormComponent } from 'src/app/shared/modules/shared-components/popup-form/popup-form.component';

@Component({
  selector: 'app-supplier-table',
  templateUrl: './supplier-table.component.html',
  styleUrls: ['./supplier-table.component.scss']
})
export class SupplierTableComponent implements OnInit {
  @Input() incomingData: any;
  @Output() changeDataFranchise=new EventEmitter()
  @ViewChild(MatPaginator, {static: true}) paginator:MatPaginator;
  @ViewChild(MatSort, {static: true}) sort:MatSort;
  @ViewChild(PopupFormComponent,{static: true}) form: PopupFormComponent;
  dataSource: any;
  iconPack= {
    edit: faEdit,
    details: faEye
  }
  columnsToDisplay: String[] = ['id','Franchise Name' ,'Supplier Name','companyName','phone','address','actions']
  allFranchises: any;
  franchiselist: any;
  franchiseSuppliers: any;
  constructor(private api: ApiWrapperService,private fb:FormBuilder) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.incomingData)
    console.log("incoming data:",this.dataSource);
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort

  }


  editHandler(row) {
    let tempRow = {...row}
    tempRow.franchiseId = row.franchiseId.name
    tempRow.franchiseName = row.franchiseId._id
    this.form.genericForm = this.fb.group({
      _id: ['',Validators.required],
      companyName: ['',[Validators.required]],
      contactNo: ['',[Validators.required]],
      franchiseName: ['',[Validators.required]],
      franchiseId: ['',[Validators.required]],
      address: ['',[Validators.required]],
      firstName: ['',[Validators.required]],
      lastName: ['',[Validators.required]],
      email: ['',[Validators.required]],
      accountNo: ['',[Validators.required]],

    })
    this.form.formLabels = [
      {name: 'companyName',placeholder: 'Company Name'},
      {name: 'contactNo',placeholder: 'Contant No'},
      {name: 'firstName',placeholder: 'First Name'},
      {name: 'lastName',placeholder: 'Last Name'},
      {name: 'address',placeholder: 'Address'},
      {name: 'accountNo',placeholder: 'Account No'},
      {name: 'email',placeholder: 'Email'},

    ]
    this.form.formFiller(tempRow)
    this.form.formTitle = 'Supplier Edit Form'
    this.form.visibleStateToggler()

  }

  submitForm(data) {
    console.log(data)
    this.api.post(environment.editSupplier,data).subscribe(res=>{
      console.log(res)
    })
    this.form.visibleStateToggler()

  }
  filterTable(filterText) {
    this.dataSource.filter = filterText
  }
  changeDataFranchiseMethod(){
    this.changeDataFranchise.emit(true);
    console.log('EventEmitter');
    this.ngOnInit();
  }
}
