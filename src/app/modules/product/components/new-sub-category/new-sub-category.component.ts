
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-new-sub-category',
  templateUrl: './new-sub-category.component.html',
  styleUrls: ['./new-sub-category.component.scss']
})
export class NewSubCategoryComponent implements OnInit {
  showStatus = false;
  categoryTypes =[  ]
  subcategoryForm: any;
  constructor(private api: ApiWrapperService, private fb:FormBuilder) { }

  ngOnInit() {
    this.subcategoryForm = this.fb.group({
      subCategory:[''],
      categoryId:['']

    });
    this.api.get(environment.findallcategories).subscribe(res => {
    console.log(res);
    this.categoryTypes = res.categories;
    });
  }
  statusChanger() {
    this.showStatus = !this.showStatus;
  }

  submit(){
this.api.post(environment.addsubcategory,this.subcategoryForm.value).subscribe(res => {
  console.log(res);
  this.ngOnInit();
  this.showStatus = !this.showStatus;
})
  }
}
