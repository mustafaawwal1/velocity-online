import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {
  categories = [
    {
      name: 'bikes',
      value: 'bike'
    },
    {
      name: 'phones',
      value: 'phone'
    }
  ]
  status = [
    {
      value: 'active',
      name: 'active'
    },
    {
      value: 'inactive',
      name: 'inactive'
    }
  ]
  subCategories = []
  productCreation: any;
  products: any;
  categoryTypes: any;
  constructor(private fb: FormBuilder , private api: ApiWrapperService, private router: Router) { }

  ngOnInit() {
    this.productCreation = this.fb.group({
      productName: ['', [Validators.required]],
      productCode: ['', [Validators.required]],
      categoryId: ['', ],
      subCategories: ['', ],
      costPrice: ['', [Validators.required]],
      minimumRetailPrice: ['', [Validators.required]],
      productTax: ['', [Validators.required]],
      unit: ['', [Validators.required]],
      status: ['', [Validators.required]],
      description: ['', [Validators.required]],
      openingStock: ['', [Validators.required]],
      imageField: [{filename:null,filetype:null,base64:null},[Validators.required] ],
      minimumQuantity: ['',[Validators.required]],
      maximumQuantity: ['',[Validators.required]]
    });

    this.api.get(environment.allProducts).subscribe(res => {
    this.products = res.products;
    console.log(res.products);

        });
    this.api.get(environment.findallcategories).subscribe(res => {
      console.log(res);
      this.categoryTypes = res.categories;
      });

      }

      submit() {

    // if (this.productCreation.subCategories.value === "") {
// this.productCreation.controls['subCategories'].
    // }

    this.api.post(environment.addGenericProducts, this.productCreation.value).subscribe( res => {
      alert('The product has been added')
    console.log(res);
    this.router.navigate(['/', 'admin', 'product', 'productList']);

        });

      }

      changeSelect(data){
        console.log("datatatat", data.subCategories);
        this.subCategories = data.subCategories;

        this.productCreation.controls['categoryId'].value=data._id;
      }

      onFileChange(event) {
        console.log('event');

        let reader = new FileReader();
        if(event.target.files && event.target.files.length > 0) {
        let file = event.target.files[0];
        reader.readAsDataURL(file);
        reader.onload = () => {
        this.productCreation.get('imageField').setValue({
        filename: file.name,
        filetype: file.type,
        base64: reader.result
        })
        console.log(this.productCreation.value);
        };
        }
        }

}
