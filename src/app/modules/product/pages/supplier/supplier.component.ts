import { FormBuilder, Validators } from '@angular/forms';
import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {
  iconPack = {
    plus: faPlus
  }
  tableData = []
  check: boolean;
  franchiseSuppliers: any;
  allFranchises: any;
  renderChecker: boolean;
  isLoading: boolean;

  constructor(private api: ApiWrapperService,private router:Router,private fb:FormBuilder) { }

  ngOnInit() {
    this.isLoading =true;
    this.check = false;

    this.api.get(environment.viewallsuppliers).subscribe(res => {
      this.tableData = res.suppliers;
      console.log(res);
      this.check = true;
      this.isLoading =false;
    });
    this.franchiseSuppliers = this.fb.group({
        franchiseId: ['', [Validators.required]],
      });

    }


}
