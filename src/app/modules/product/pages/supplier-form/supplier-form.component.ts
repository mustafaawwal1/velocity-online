import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-supplier-form',
  templateUrl: './supplier-form.component.html',
  styleUrls: ['./supplier-form.component.scss']
})
export class SupplierFormComponent implements OnInit {
  supplierForm: any;
  allFranchises: any;

  constructor(private fb: FormBuilder, private api: ApiWrapperService,private router:Router) { }

  ngOnInit() {
    this.supplierForm = this.fb.group({
      franchiseId: ['', [Validators.required]],
      firstName:['',[Validators.required]],
      lastName:['',[Validators.required]],
      companyName:['',[Validators.required]],
      email:['',[Validators.required,Validators.email]],
      phone:['',[Validators.required]],
      address:['',[Validators.required]],
      accountNo:['',[Validators.required]],

    });
    this.api.get(environment.allFranchises).subscribe(response => {
      console.log(response.franchises);
      this.allFranchises = response.franchises;

    });

  }
  submit() {
    console.log(this.supplierForm.value);
    this.api.post(environment.addsuppliers,this.supplierForm.value).subscribe(res => {
      console.log(res);
      this.router.navigate(['admin','product','productList','supplier']);
    });
  }

}

