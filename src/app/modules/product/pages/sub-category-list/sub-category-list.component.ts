import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sub-category-list',
  templateUrl: './sub-category-list.component.html',
  styleUrls: ['./sub-category-list.component.scss']
})
export class SubCategoryListComponent implements OnInit {
  iconPack = {
    plus: faPlus
  };
  tableData = [ ];
  checker: boolean;
  isLoading: boolean;
  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.isLoading = true;
    this.checker = false;
    this.api.get(environment.findallcategories).subscribe(res => {
  this.tableData = res.categories;
  this.checker = true;
  this.isLoading = false;
 });
  }

}
