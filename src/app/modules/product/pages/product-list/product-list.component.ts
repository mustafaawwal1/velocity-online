import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  iconPack = {
    plus: faPlus
  }
  productList: any;
  check: boolean=false;
  isLoading: boolean;

  constructor(private api: ApiWrapperService) { }

  ngOnInit() {
    this.isLoading =true
    this.check = false;
    this.api.get(environment.allProducts).subscribe(res => {
      this.productList = res.products;
      console.log(this.productList);
      this.check=true;
      this.isLoading =false;
    });
  }
  createHandlerFunction() {
    console.log('something')
  }
}
