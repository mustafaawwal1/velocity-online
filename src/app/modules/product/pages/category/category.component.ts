import { environment } from './../../../../../environments/environment';
import { ApiWrapperService } from './../../../../SercicesAndMiddlewares/apiwrapperservice';
import { Component, OnInit } from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  iconPack = {
    plus: faPlus
  }
  tableData = [
    {
      id: '1',
      name: 'Bikes',
      subCategory: ''
    }
  ]
  checker: boolean;
  isLoading: boolean;
  constructor(private api:ApiWrapperService) { }

  ngOnInit() {
    this.isLoading =true;
    this.checker =false;
    this.api.get(environment.findallcategories).subscribe(res => {
      this.tableData = res.categories;
      this.checker = true;
      this.isLoading = false;
    });
  }

}
