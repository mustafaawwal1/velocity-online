import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { ApiWrapperService } from 'src/app/SercicesAndMiddlewares/apiwrapperservice';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoading: boolean;
  constructor(private fb: FormBuilder, private router: Router, public api: ApiWrapperService, private _store: Store<any>) {
    _store.select('states').subscribe(userData => {
      this.userData = userData;
    });

    }

  loginForm: FormGroup;
  userData: any;

  private isAuthenticated = false;
  private token: string;
  private tokenTimer: any;
  private authStatusListener = new Subject<boolean>();

  ngOnInit() {
    this.isAuthenticated = this.userData.loggedin;
    console.log(this.isAuthenticated);
    if (this.getIsAuth()) {
      this.router.navigate(['/', 'admin', 'dashboard', 'home']);
    }
    this.createForm();

  }
  createForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(7)]]
    });
  }
  login() {

      if (this.loginForm.invalid) {
        return;
      }
      this.isLoading = true;
      this.loginForm.value.storeId = '5de7732bc2c4210333219810';
      this.api.post( environment.login, this.loginForm.value).subscribe(
        response => {
          console.log(response);

          this.isLoading = false;
          const token = response.token;
          this.token = token;
          if (token) {


          this.isAuthenticated = true;
          this.authStatusListener.next(true);
          this.addUserData(response);
          const now = new Date();


          localStorage.setItem('token', this.token);
          if (response.user.role.roleName === 'store admin' || response.user.role.roleName === 'franchise admin' || response.user.role.roleName === 'accountant' ) {
            this.router.navigate(['/', 'admin', 'dashboard', 'home']);
          }



        }
      }, (err: any) => {
        console.log('error_message : ', err.error);
        this.authStatusListener.next(false);
      });

  }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }
  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }



  addUserData(userData) {
    console.log(userData);
    this._store.dispatch({
      type: 'LOGIN',
      payload: userData
    });
  }
  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/']);
  }


  private setAuthTimer(duration: number) {
    console.log('Setting timer: ' + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }



  private clearAuthData() {
    localStorage.removeItem('token');

  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!token || !expirationDate) {
      return;
    }
    return {
      token,
      expirationDate: new Date(expirationDate)
    };
  }
}





