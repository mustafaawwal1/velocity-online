import { Component, OnInit } from '@angular/core';
import { faBoxes, faRulerVertical ,faMoneyBill, faHandHoldingUsd, faShoppingCart, faWrench, faChartPie, faTag, faTags, faIndustry, faChartLine, faRocket } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  statCards: any;
  constructor(private _store: Store<any>) {
    _store.select('states').subscribe(userData => {
      this.userData = userData;
    });
  }
  userData: any;
  roleName: any;


  ngOnInit() {
    this.roleName   = this.userData.userData.user.role.roleName;

    if (this.roleName == 'store admin') {
      this.statCards = [
        {
          icon: faBoxes,
          heading: 'Products',
          background: '#00BCA4',
          stat: [
            {
              statName: 'Total Products',
              statValue: 12
            },
            {
              statName: 'Zero Products',
              statValue: 32
            }
          ]
        },
        {
          icon: faWrench,
          heading: 'Settings',
          background: '#984DFF',
          stat: [
            {
              statName: 'Total Store Users',
              statValue: 29
            },
            {
              statName: 'Total Store Branches',
              statValue: 9
            }
          ]
        }, {
              icon: faRulerVertical,
              heading: 'View/Create  Suppliers',
              routerLink: ['../../../../product', 'productList','supplier'],
              background: '#E67E22',
              stat: [
                {
                  statName: 'Total Products',
                  statValue: 12
                },
                {
                  statName: 'Zero Products',
                  statValue: 32
                }
              ]
            },



      ];
    }


    if (this.roleName == 'franchise admin'){
      this.statCards = [

          {
            icon: faHandHoldingUsd,
            heading: 'Purchase',
            background: ' #FFA500',
            stat: [
              {
                statName: 'Total Purchases',
                statValue: 213123
              }
            ]
          },
          {
            icon: faShoppingCart,
            heading: 'Sales',
            background: '#65A6FF',
            stat: [
              {
                statName: 'Total Sales',
                statValue: 213213
              }
            ]
          },
          {
            icon: faMoneyBill,
            routerLink: ['../../../../pos', 'screen'],
            heading: 'POS Screen',
            background: '#228B22',
            stat: [
              // {
              //   statName: 'Total Sales',
              //   statValue: 213213
              // }
            ]
          },

        ]
    }


    if (this.roleName == 'accountant') {
      this.statCards=[
        {
              icon: faChartLine,
              routerLink: ['../../../../accounts', 'accountList','customerLedger' ],
              heading: 'Customer Ledger',
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
            {
              icon: faChartLine,
              heading: 'Supplier Ledger',
              routerLink: ['../../../../accounts', 'accountList','supplierLedger' ],
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
            {
              icon: faChartLine,
              heading: 'Voucher',
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
            {
              icon: faChartLine,
              heading: 'Transaction',
              routerLink: ['../../../../accounts', 'accountList','transactions' ],
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
            {
              icon: faChartLine,
              routerLink: ['../../../../accounts', 'accountList' ],
              heading: 'Expense',
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
            {
              icon: faChartLine,
              heading: 'Profit and Loss',
              background: '#00008b',
              stat: [
                // {
                //   statName: 'Total Accoun',
                //   statValue: 12,
                // },
              ]
            },
       ]
    }

  }
  onClick(heading) {

if (heading == 'Products'){
  this.statCards = [
      {
        icon: faBoxes,
        routerLink: ['../../../../product', 'productList'],
        heading: 'View Product List',
        background: '#00BCA4',
        stat: [
          // {
          //   statName: 'Total Products',
          //   statValue: 12
          // },
        ]
      },
      {
        icon: faBoxes,
        heading: 'Create New Product',
        routerLink: ['../../../../product', 'productList','newProduct'],
        background: '#00BCA4',
        stat: [
          // {
          //   statName: 'Total Products',
          //   statValue: 12
          // },
        ]
      },
      {
        icon: faBoxes,
        heading: 'Print Bar Code',
        routerLink: ['../../../../product', 'productList','barcode'],
        background: '#00BCA4',
        stat: [
          // {
          //   statName: 'Total Products',
          //   statValue: 12
          // },
        ]
      },
      {
        icon: faBoxes,
        heading: 'Category',
        routerLink: ['../../../../product', 'productList','category'],
        background: '#00BCA4',
        stat: [
          // {
          //   statName: 'Total Products',
          //   statValue: 12
          // },
        ]
      },
      {
        icon: faBoxes,
        heading: 'Sub Category',
        routerLink: ['../../../../product', 'productList','subcategory'],
        background: '#00BCA4',
        stat: [
          // {
          //   statName: 'Total Products',
          //   statValue: 12
          // },
        ]
      },
  ]
}

if (heading == 'Settings'){
  this.statCards=[
    {
    icon: faWrench,
    heading: 'General Settings',
    routerLink: ['../../../../settings', 'generalSettings'],
    background: '#984DFF',
    stat: [
      // {
      //   statName: 'Total Store Users',
      //   statValue: 29
      // },
      // {
      //   statName: 'Total Store Branches',
      //   statValue: 9
      // }
    ]
  },
  {
    icon: faWrench,
    heading: 'View/Create New User',
    routerLink: ['../../../../settings', 'generalSettings' ,'user'],
    background: '#984DFF',
    stat: [
      // {
      //   statName: 'Total Store Users',
      //   statValue: 29
      // },
      // {
      //   statName: 'Total Store Branches',
      //   statValue: 9
      // }
    ]
  },
  {
    icon: faWrench,
    heading: 'View/Create New Branch',
    routerLink: ['../../../../settings', 'generalSettings' , 'branch'],
    background: '#984DFF',
    stat: [
      // {
      //   statName: 'Total Store Users',
      //   statValue: 29
      // },
      // {
      //   statName: 'Total Store Branches',
      //   statValue: 9
      // }
    ]
  },

]
}


if(heading == 'Purchase'){
  this.statCards = [

    {
      icon: faHandHoldingUsd,
      routerLink: ['../../../../purchase', 'purchaseList' ],
      heading: 'Purchase List',
      background: ' #FFA500',
      stat: [
        // {
        //   statName: 'Total Purchases',
        //   statValue: 213123
        // }
      ]
    },{
      icon: faHandHoldingUsd,
      heading: 'Create New Purchase',
      routerLink: ['../../../../purchase', 'purchaseList','newPurchase' ],
      background: ' #FFA500',
      stat: [
        // {
        //   statName: 'Total Purchases',
        //   statValue: 213123
        // }
      ]
    },
    {
      icon: faHandHoldingUsd,
      heading: 'Purchase Report',
      background: ' #FFA500',
      stat: [
        // {
        //   statName: 'Total Purchases',
        //   statValue: 213123
        // }
      ]
    },
  ]
}
if (heading == 'Sales'){
  this.statCards=[
    {
      icon: faShoppingCart,
      routerLink: ['../../../../sales', 'salesList' ],
      heading: 'Sales Register',
      background: '#65A6FF',
      stat: [
        // {
        //   statName: 'Total Sales',
        //   statValue: 213213
        // }
      ]
    },
    {
      icon: faShoppingCart,
      routerLink: ['../../../../sales', 'salesList','newSale' ],
      heading: 'Create New sale',
      background: '#65A6FF',
      stat: [
        // {
        //   statName: 'Total Sales',
        //   statValue: 213213
        // }
      ]
    },

  ]

}

if (heading == 'Accountant'){

}


  }

  // statCards = [
  //   {
  //     icon: faBoxes,
  //     heading: 'Product',
  //     background: '#00BCA4',
  //     stat: [
  //       {
  //         statName: 'Total Products',
  //         statValue: 12
  //       },
  //       {
  //         statName: 'Zero Products',
  //         statValue: 32
  //       }
  //     ]
  //   },
  //   {
  //     icon: faHandHoldingUsd,
  //     heading: 'Purchase',
  //     background: '#984DFF',
  //     stat: [
  //       {
  //         statName: 'Total Purchases',
  //         statValue: 213123
  //       }
  //     ]
  //   },
  //   {
  //     icon: faShoppingCart,
  //     heading: 'Sales',
  //     background: '#65A6FF',
  //     stat: [
  //       {
  //         statName: 'Total Sales',
  //         statValue: 213213
  //       }
  //     ]
  //   },
  //   {
  //     icon: faChartPie,
  //     heading: 'Stock',
  //     background: '#E67E22',
  //     stat: [
  //       {
  //         statName: 'Total Stock',
  //         statValue: 12312312
  //       }
  //     ]
  //   },
  //   {
  //     icon: faIndustry,
  //     heading: 'Branch',
  //     background: '#2D2D2D',
  //     stat: [
  //       {
  //         statName: 'Total Branches',
  //         statValue: 12,
  //       },
  //       {
  //         statName: 'Open Branches',
  //         statValue: 6
  //       },
  //       {
  //         statName: 'Cloesed Branches',
  //         statValue: 6
  //       },
  //     ]
  //   },
  //   {
  //     icon: faChartLine,
  //     heading: 'Profit',
  //     background: '#DB3B8A',
  //     stat: [
  //       {
  //         statName: 'Total profit',
  //         statValue: 12,
  //       },
  //     ]
  //   },
  //   // {
  //   //   icon: faRocket,
  //   //   heading: 'Reactive Store',
  //   //   background: 'seagreen'
  //   // }
  // ]




}
