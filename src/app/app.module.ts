import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/modules/material/material.module';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { MainRoutes } from './main.routes';
import { SecondaryOutletComponent } from './shared/components/secondary-outlet/secondary-outlet.component';
import { SharedNavModule } from './shared/modules/shared-nav/shared-nav.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ActionReducerMap, ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import {states} from './shared/redux/reducer';
import { ApiWrapperService } from './SercicesAndMiddlewares/apiwrapperservice';
const reducers: ActionReducerMap<any> = {states};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: ['states'],
    rehydrate: true
  })(reducer);
}
const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    SecondaryOutletComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    StoreModule.forRoot(
      reducers, {metaReducers}
    ),
    RouterModule.forRoot(MainRoutes, {preloadingStrategy: PreloadAllModules}),
    SharedNavModule
  ],
  providers: [ApiWrapperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
